package au.kapsch.iot.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import au.kapsch.iot.dao.IoTRepository;

@SpringBootTest(
    properties = {
      "spring.cloud.config.enabled: false"
    })
class IOTServiceUnitTest {

  @MockBean IoTRepository repository;

  @Test
  void fetchAndProcessDataTest() {
//    Mockito.when(repository.fetchData())
//        .thenReturn(IoTDto.builder().data("Mockito data").build());
//    Assert.assertEquals("Mockito data processed.", this.service.processData().getData());
  }
}
