package au.kapsch.iot.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/** Custom rest error response. */
@Data
@Builder
public class IoTErrorResponse {

    /** Time stamp. */
    private LocalDateTime timestamp;
    /** Custom status. */
    private String status;
    /** Exception message. */
    private String error;
    /** Custom internationalized message. */
    private List<IoTErrorMessage> message;
    /** Request uri. */
    private String path;
    /** Field name */
    private String field;

}
