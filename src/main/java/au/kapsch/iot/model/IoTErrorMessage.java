package au.kapsch.iot.model;

import lombok.Builder;
import lombok.Data;

/** Custom error message */
@Data
@Builder
public class IoTErrorMessage {
    private String language;
    private String value;
}
