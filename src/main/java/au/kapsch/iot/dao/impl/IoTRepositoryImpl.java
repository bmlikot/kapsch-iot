package au.kapsch.iot.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import au.kapsch.iot.dao.IoTRepository;
import au.kapsch.iot.dao.MeasurementRepository;
import au.kapsch.iot.dao.dto.Measurement;
import lombok.AllArgsConstructor;

@Repository
@AllArgsConstructor
public class IoTRepositoryImpl implements IoTRepository {
	private final MeasurementRepository measurementRepository;

	public List<Measurement> fetchData() {
		return this.measurementRepository.findAll();
	}
}
