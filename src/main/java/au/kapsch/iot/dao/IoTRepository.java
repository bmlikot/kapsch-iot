package au.kapsch.iot.dao;

import java.util.List;

import au.kapsch.iot.dao.dto.Measurement;

/** IoT repository. */
public interface IoTRepository {
	/**
	 * Method to fetch data.
	 */
	public List<Measurement> fetchData();
}
