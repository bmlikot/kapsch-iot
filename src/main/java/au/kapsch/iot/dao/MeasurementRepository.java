package au.kapsch.iot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import au.kapsch.iot.dao.dto.Measurement;

public interface MeasurementRepository extends JpaRepository<Measurement, Long> {

}
