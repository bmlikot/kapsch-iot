package au.kapsch.iot.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import au.kapsch.iot.api.v1.ApiApiDelegate;
import au.kapsch.iot.api.v1.model.IoTModel;
import au.kapsch.iot.dao.IoTRepository;
import au.kapsch.iot.dao.dto.Measurement;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class IoTServiceImpl implements ApiApiDelegate {

	private final IoTRepository repository;

	@Override
	public ResponseEntity<List<IoTModel>> listItems() {
		List<Measurement> fetchedData = this.repository.fetchData();

		if (CollectionUtils.isEmpty(fetchedData))
			return null;

		List<IoTModel> ret = fetchedData.stream().map(data -> IoTModel.builder().createdTime(data.getCreatedTime())
				.device(data.getDevice()).name(data.getName()).value(data.getValue().longValue()).build())
				.collect(Collectors.toList());

		return new ResponseEntity<>(ret, HttpStatus.OK);

	}
}
