package au.kapsch.iot.controller;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import au.kapsch.iot.model.IoTErrorResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@AllArgsConstructor
@Slf4j
public class WebRestControllerAdvice {


    /**
     * Handles general exceptions.
     *
     * @param ex
     * @param httpRequest
     * @return
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public IoTErrorResponse customException(final Throwable ex, final HttpServletRequest httpRequest) {
    	// TODO return localized message
    	log.error("", ex);
        return IoTErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .error(ex.getMessage())
                .path(httpRequest != null ? httpRequest.getRequestURI() : null)
                .build();
    }


    /**
     * Handles Unchecked exceptions.
     *
     * @param ex
     * @param httpRequest
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public IoTErrorResponse uncheckedException(final RuntimeException ex, final HttpServletRequest httpRequest) {
    	// TODO return localized message
    	log.error("", ex);
        return IoTErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .error(ex.getMessage())
                .path(httpRequest != null ? httpRequest.getRequestURI() : null)
                .build();
    }
}
