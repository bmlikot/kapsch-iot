package au.kapsch.iot.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.kapsch.iot.api.v1.model.IoTModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * IoT Controller.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api")
@Slf4j
public class IoTController {

  /**
   * Update an item information.
   *
   * @return Updated item.
   */
  @PutMapping
  @ApiOperation(
      value = "Update an item information",
      notes = "Returns updated item",
      response = IoTModel.class)
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Successfully updated item information"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(
            code = 403,
            message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 500, message = "Internal error")
      })
  public IoTModel updateItem(@RequestBody IoTModel item) {
    log.debug(" ---> Update item: {}" + item);
    return null;
  }

  /** Deletes specific item with the supplied item id. */
  @DeleteMapping("/{id}")
  @ApiOperation(value = "Deletes specific item with the supplied item id", notes = "Returns void")
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Successfully deletes the specific item"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(
            code = 403,
            message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 500, message = "Internal error")
      })
  public void delete(@ApiParam(value = "Item id.", required = true) @PathVariable("id") Long id) {
    log.debug(" ---> Delete item with id: {}" + id);
  }
}
