![](https://img.shields.io/badge/spring%20boot%202-compatible-green.svg)

# Kapsch Microservice

### Requirements

- Latest openJDK 11

> https://developers.redhat.com/products/openjdk/download

### Eclipse Setup

# Lombok
Go to `C:\Users\<myusername>\.m2\repository\org\projectlombok\lombok\<version>`, e.g. `C:\Users\<myusername>\.m2\repository\org\projectlombok\lombok\1.18.16` and run lombok-1.18.16.jar with double click to install the plugin. After installing the plugin, we need to restart the IDE.

> https://www.baeldung.com/lombok-ide

### Usage

Run as Spring Boot App.

Go to:

> http://localhost:8080/kapsch-iot/api

> http://localhost:8080/kapsch-iot/swagger-ui/

> http://localhost:8080/kapsch-iot/actuator/health

> http://localhost:8080/kapsch-iot/actuator/info

> POST http://localhost:8080/kapsch-iot/actuator/refresh

### Test

Execute:

```
mvn clean test
```

If everything went okay, go to:

> kapsch-iot\target\site\jacoco-ut\index.html

